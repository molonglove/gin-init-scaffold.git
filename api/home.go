package api

import (
	"github.com/gin-gonic/gin"
	"go_init/service"
	"go_init/view"
	"net/http"
)

type HomeController struct {
	HomeService *service.HomeService
}

func NewHomeController() *HomeController {
	return &HomeController{HomeService: service.NewHomeService()}
}

func (h *HomeController) HomeIndex(c *gin.Context) {
	homes, err := h.HomeService.Select()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": "400400400", "msg": err.Error()})
	}
	c.JSON(http.StatusOK, homes)
}

func (h *HomeController) HomeAdd(c *gin.Context) {
	var params view.HomeAddReq
	if err := c.ShouldBind(&params); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": "400400400", "msg": err.Error()})
	}
	if err := h.HomeService.Add(params); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": "2020202", "msg": "添加数据失败"})
	}
	c.JSON(http.StatusOK, gin.H{"code": "2020202", "msg": "添加数据成功"})
}

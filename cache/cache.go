package cache


type Cache interface {
	Set(key string, value interface{}, time int) (bool, error)
	Exists(key string) bool
	Get(key string) ([]byte, error)
	Delete(key string) (bool, error)
	BlurryDel(key string) error
}

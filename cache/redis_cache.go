package cache

import (
	"encoding/json"
	"fmt"
	"github.com/gomodule/redigo/redis"
)

var (
	SET = "SET"
	EXISTS = "EXISTS"
	GET = "GET"
	EXPIRE = "EXPIRE"
	DEL = "DEL"
	KEYS = "KEYS"
)

type RedisPool struct {
	Pool redis.Pool
}

// Conn 获取数据连接
func (p *RedisPool) conn() redis.Conn {
	return p.Pool.Get()
}

// Set 设置数据
func (p *RedisPool) Set(key string, value interface{}, time int) (bool, error) {
	conn := p.conn()
	data, err := json.Marshal(value)
	if err != nil {
		return false, err
	}
	if _, err = conn.Do(SET, key, data); err != nil {
		return false, err
	}
	_, _ = conn.Do(EXPIRE, key, time)
	return true, nil
}

// Exists 是否存在
func (p *RedisPool) Exists(key string) bool {
	conn := p.conn()
	flag, err := redis.Bool(conn.Do(EXISTS, key))
	if err != nil {
		return false
	}
	return flag
}

// Get 获取数据
func (p *RedisPool) Get(key string) ([]byte, error) {
	conn := p.conn()
	data, err := redis.Bytes(conn.Do(GET, key))
	if err != nil {
		return nil, err
	}
	return data, err
}

// Delete 删除
func (p *RedisPool) Delete(key string) (bool, error) {
	conn := p.conn()
	return redis.Bool(conn.Do(DEL, key))
}

// BlurryDel 模糊删除
func (p *RedisPool) BlurryDel(key string) error {
	conn := p.conn()
	keys, err := redis.Strings(conn.Do(KEYS, fmt.Sprintf("*%s*", key)))
	if err != nil {
		return err
	}
	for _, key := range keys {
		p.Delete(key)
	}
	return nil
}

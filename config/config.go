package config

import (
	"fmt"
	"github.com/BurntSushi/toml"
)

type Config struct {
	Server  Server
	Mongodb Mongodb
	Jwt     Jwt
	Redis   Redis
	Logger  Logger
}

type Captcha struct {
	KeyLong   int `yaml:"key-long"`
	ImgWidth  int `yaml:"img-width"`
	ImgHeight int `yaml:"img-height"`
}

type Redis struct {
	Dial      string   `toml:"dial"`
	Ip        string   `toml:"ip"`
	Part      string   `toml:"part"`
	Password  string   `toml:"password"`
	Database  int      `toml:"database"`
	MaxIdle   int      `toml:"max-idle"`
	MaxActive int      `toml:"max-active"`
}

type Mongodb struct {
	Ip       string
	Part     int64
	Database string
}

func (m *Mongodb) Link() string {
	return fmt.Sprintf("mongodb://%s:%d", m.Ip, m.Part)
}

type Server struct {
	Part   string `toml:"part"`   // 地址
	Status string `toml:"status"` // 状态
}

type Jwt struct {
	SignKey     string `toml:"sign_key"`
	ExpiresTime int64  `toml:"expires_time"`
	Issuer      string
}

type Logger struct {
	FilePath   string `toml:"file_path"`
	FileName   string `toml:"file_name"` // 日志文件路径
	MaxSize    int    `toml:"max_size"`
	MaxAge     int    `toml:"max_age"`
	MaxBackups int    `toml:"max_backups"`
}

// 默认的配置文件路由
const ConfigPath = "./config/config.toml"

func InitConfig(args ...string) Config {
	configPath := ConfigPath
	if len(args) == 1 && args[0] != "" {
		configPath = args[0]
	}
	var config Config
	if _, err := toml.DecodeFile(configPath, &config); err != nil {
		panic("config.toml配置文件读取失败:" + err.Error())
	}
	return config
}

package models

// Home 实体
type Home struct {
	Id    int64  `json:"id"`
	Name  string `json:"name"`
	Age   int64  `json:"age"`
	Brief string `json:"brief"`
}

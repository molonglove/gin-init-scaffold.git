package repository

import (
	"context"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go_init/global"
	"go_init/models"
	"go_init/utils/tools"
)

type HomeRepository struct {}

func NewHomeRepository() *HomeRepository {
	return &HomeRepository{}
}

// FindAll 查找数据
func (h *HomeRepository) FindAll() ([]models.Home, error) {
	var result []models.Home
	cur, err := global.DB.Collection("student").Find(context.TODO(), bson.D{})
	if err != nil {
		return nil, err
	}
	defer cur.Close(context.TODO())
	for cur.Next(context.TODO()) {
		var temp models.Home
		if err := cur.Decode(&temp); err == nil {
			result = append(result, temp)
		}
	}
	return result, nil
}

// InsertOne 增加一个数据
func (h *HomeRepository) InsertOne(data interface{}) error {
	var home models.Home
	if err := tools.StructCopy(data, &home); err != nil {
		return err
	}
	result, err := global.DB.Collection("student").InsertOne(context.TODO(), home)
	logrus.Infof("插入数据：%v", result)
	return err
}



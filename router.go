package main

import (
	"github.com/gin-gonic/gin"
	"go_init/api"
	"go_init/middleware"
)

func InitRouter(engine *gin.Engine) *gin.Engine {
	engine.Use(middleware.CorsMiddle())
	engine.Use(middleware.LogMiddle())
	group := engine.Group("api")
	{
		HomeRouter(group)
	}
	return engine
}



func HomeRouter(g *gin.RouterGroup) {
	homeController := api.NewHomeController()
	home := g.Group("home")
	{
		home.POST("add", homeController.HomeAdd)
		home.GET("list", homeController.HomeIndex)
	}
}
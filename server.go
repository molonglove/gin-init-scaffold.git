package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go_init/config"
	"go_init/global"
	"os"
	"strings"
)

type Server interface {
	ListenAndServe() error
}

// parse 解析参数
func parse(args []string) string {
	if len(args) > 1 {
		argArr := strings.Split(os.Args[1], "=")
		if len(argArr) == 2 && argArr[0] == "--config" {
			return argArr[1]
		}
	}
	return ""
}

func main() {
	// 初始化配置
	global.Config = config.InitConfig(parse(os.Args))
	global.NewConfig()
	// 关闭连接
	defer func(Mongo *mongo.Client, ctx context.Context) {
		err := Mongo.Disconnect(ctx)
		if err != nil {
			panic("关闭连接失败")
		}
	}(global.Mongo, context.TODO())
	// 初始化路由
	router := InitRouter(gin.Default())
	InitServer(router).
		ListenAndServe().
		Error()
}

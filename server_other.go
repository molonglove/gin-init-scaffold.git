// +build !windows

package main

import (
	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"
	"go_init/global"
	"time"
)

func InitServer(router *gin.Engine) Server {
	gin.SetMode(gin.ReleaseMode)
	s := endless.NewServer(global.Config.Server.Part, router)
	s.ReadHeaderTimeout = 10 * time.Millisecond
	s.WriteTimeout = 10 * time.Second
	s.MaxHeaderBytes = 1 << 20
	return s
}

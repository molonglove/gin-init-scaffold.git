package message

const (
	SUCCESS = "001001200"
	FAILED = "001001400"
)

var CodeMsg = map[string]string{
	SUCCESS: "操作成功",
	FAILED: "操作失败",
}

package message

type ResultMsg struct {
	Code string `json:"code"`
	Msg string `json:"msg"`
	Data interface{} `json:"data"`
}

// NewResultMsg 创建消息
func NewResultMsg() *ResultMsg {
	return new(ResultMsg)
}

// Success 成功
func (r *ResultMsg) Success(data interface{}) *ResultMsg {
	r.Msg = SUCCESS
	r.Code = CodeMsg[SUCCESS]
	r.Data = data
	return r
}

// Failed 失败
func (r *ResultMsg) Failed(detailMsg string) *ResultMsg {
	r.Msg = CodeMsg[FAILED]
	r.Code = FAILED
	r.Data = detailMsg
	return r
}


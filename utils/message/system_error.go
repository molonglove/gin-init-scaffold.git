package message

import "fmt"

type SystemError struct {
	msg string
}

func (s *SystemError) Error() string {
	return fmt.Sprintf("发生异常：%s", s.msg)
}

func NewSysError(msg string) SystemError {
	return SystemError{
		msg: msg,
	}
}

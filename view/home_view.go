package view

type HomeAddReq struct {
	Id    int64  `json:"id"`
	Name  string `json:"name"`
	Age   int64  `json:"age"`
	Brief string `json:"brief"`
}
